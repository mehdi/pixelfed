#!/bin/bash

# Place custom startup commands here
cd /app/data
readonly ARTISAN="sudo -E -u www-data php /app/data/artisan"

$ARTISAN config:cache
$ARTISAN route:cache
$ARTISAN view:cache
$ARTISAN migrate --force
$ARTISAN update
$ARTISAN horizon &
