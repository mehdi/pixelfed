#!/bin/bash

set -eu

mkdir -p /app/data/public /run/apache2 /run/cron /run/app/sessions /run/postfix
readonly ARTISAN="sudo -E -u www-data php /app/data/artisan"
mkdir -p /run/pixelfed/sessions /run/pixelfed/bootstrap-cache /run/pixelfed/framework-cache /run/pixelfed/logs /run/pixelfed/cache /run/postfix/maildrop
chown -R www-data:www-data /run/postfix

if [[ ! -f "/app/data/php.ini" ]]; then
    echo "==> Generating php.ini"
    cp /etc/php/7.3/apache2/php.ini.orig /app/data/php.ini
else
    crudini --set /app/data/php.ini Session session.gc_probability 1
    crudini --set /app/data/php.ini Session session.gc_divisor 100
fi

[[ ! -f /app/data/crontab ]] && cp /app/code/crontab.template /app/data/crontab
## configure in-container Crontab
if ! (env; cat /app/data/crontab; echo -e '\nMAILTO=""') | crontab -u www-data -; then
    echo "==> Error importing crontab. Continuing anyway"
else
    echo "==> Imported crontab"
fi

chown -R www-data:www-data /app/data /run/apache2 /run/app /tmp

#Fresh startup, initialize
if ! [ -f /app/data/.initialized ]; then
    cd /app/data/
    echo "Fresh installation, setting up data directory..."
    cp /app/code/pkg.template/.env /app/data/.env
    cp -R /app/code/pkg.template/* /app/data/
    chown -R www-data:www-data /app/data
    chmod +x run.sh artisan
    $ARTISAN storage:link 
    $ARTISAN horizon:install 
    $ARTISAN horizon:assets
    $ARTISAN key:generate
    $ARTISAN migrate --force

  # Flag the container has been initialized
  touch /app/data/.initialized
fi

#regular startup
[[ -d /app/data/storage/framework/sessions ]] && rm -rf /app/data/storage/framework/sessions
ln -sf /run/pixelfed/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/pixelfed/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/pixelfed/logs /app/data/storage/logs
$ARTISAN config:cache
$ARTISAN route:cache
$ARTISAN view:cache
$ARTISAN migrate --force
$ARTISAN update
#$ARTISAN horizon &

echo "==> Starting Pixelfed"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Lamp
